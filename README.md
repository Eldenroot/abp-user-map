# ABP User Map

Creates a new page on MyBB showing the user map

# Features
## Usermap page
A simple page showing the map and clusters/markers. Clicking a marker will show the username and his avatar.

## ACP
* choose the default location (the center of the map when initialised)
* choose the default zoom level
* choose the marker

## User CP
The subpage "your location" will allow the user to place/move/delete a marker on his location.

# Installation - Upgrade
* Unzip the archive
* Upload the content of the Upload directory to your forum root
* **New installation** : install the plugin from your ACP
* **Upgrade** : deactivate and reactivate the plugin from your ACP

**Don not forget**: There is two configurations, one in the normal plugin setting way and the other (dedicated to the map itself) in a separate admin page.

# Ressources
* [Open Street Map](https://www.openstreetmap.org/)
* [Leaflet](https://leafletjs.com/)

# TODO

* [x]  Add top link

* [x]  Add WOL informations

* [ ]  *peharps* : choose country and town with a simple form ?
