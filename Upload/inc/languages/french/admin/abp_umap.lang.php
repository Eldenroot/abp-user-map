<?php
/* General section */
$l['abp_umapname'] = 'Carte utilisateurs';
$l['abp_umapdesc'] = 'Une carte pour visualiser la localisation de vos utilisateurs';

/* Settings */
$l['abp_umap_gallowed'] = 'Groupes autorisés';
$l['abp_umap_gallowed_desc'] = 'Groupes autorisés à visualiser la carte des utilisateurs';
$l['abp_umap_adisplay'] = 'Avatar';
$l['abp_umap_adisplay_desc'] = 'Afficher l\'avatar dans la popup ?';
$l['abp_umap_ulink'] = 'Liens';
$l['abp_umap_ulink_desc'] = 'Mettre un lien vers le profil dans la popup ?';
$l['abp_umap_gcolor'] = 'Couleur';
$l['abp_umap_gcolor_desc'] = 'Appliquer la couleur de son groupe à l\'utilisateur ?';

// Old settings
$l['abp_umap_setting_title'] = 'Réglages UserMap';
$l['abp_umap_setting_desc'] = 'Différents réglages initiaux pour la carte';

$l['abp_umap_latlng'] = 'Emplacement par défaut';
$l['abp_umap_latlng_desc'] = 'Sélectionner la position par défaut du marqueur en le déplaçant sur la carte';

$l['abp_umap_zoom'] = 'Zoom par défaut';
$l['abp_umap_zoom_desc'] = 'Sélectionner la valeur initiale du zoom (1 = monde, 19 = rue)';

$l['abp_umap_marker'] = 'Marqueur';
$l['abp_umap_marker_desc'] = 'Emplacement du fichier de marqueur avec le / initial (.png / 32px * 32px est recommandé)';

$l['abp_umap_btn_save'] = 'Valider';