<?php

$l['abp_umapname'] = 'User Map';

$l['abp_umap_btn_update'] = 'Update';
$l['abp_umap_btn_delete'] = 'Delete';
$l['abp_umap_updated'] = 'Your location is saved';
$l['abp_umap_deleted'] = 'Your location is deleted';
$l['abp_umap_what'] = 'I did not understand your action';
$l['abp_umap_ucpdesc'] = 'Move the pin to your location and click the submit button. You will appear on the <a href="./misc.php?action=abp_umap" >Usermap</a>.';
$l['abp_umap_confirm'] = 'Confirmation :';
$l['abp_umap_updateme'] = 'Check me to allow the submission of the modification';
$l['abp_umap_deleteme'] = 'Check me to allow the deletion of my location';
$l['abp_umap_cannotdel'] = 'You can not update and delete !';

$l['abp_umap_title_update'] = 'Update my location';
$l['abp_umap_title_delete'] = 'Delete my location';

$l['abp_umap_wol'] = 'Looking the <a href="{1}">user map</a>';
$l['osm_credit'] = '&copy; <a href="https://www.openstreetmap.org" title="OpenStreetMap">OpenStreetMap</a> contributors';

$l['abp_umap_page_title'] = 'Map of the users';
$l['abp_umap_stats'] = 'Currently, {1} users have set their localisation.';
$l['abp_umap_centerme'] = '<li><a href="#" id="centerme">Center the map on me</a></li>';

$l['abp_umap_ucp_notice_title'] = 'My location on map';
$l['abp_umap_ucp_notice'] = 'Here, you can indicate your localisation easily.<br />Simply drag the marker, then check the confirmation and submit. You can zoom the map if you want to be more pecise.<br />You can also delete your localisation if you do not want to show others where you are.';
$l['abp_umap_location_not_set'] = '<div class="red_alert">You did not set your localisation</div>';