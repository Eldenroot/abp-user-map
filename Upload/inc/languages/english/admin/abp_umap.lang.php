<?php
/* General section */
$l['abp_umapname'] = 'User Map';
$l['abp_umapdesc'] = 'A map to locate users';

/* Settings */
$l['abp_umap_gallowed'] = 'Allowed groups';
$l['abp_umap_gallowed_desc'] = 'Groups allowed to access to the UserMap page';
$l['abp_umap_adisplay'] = 'Display avatar';
$l['abp_umap_adisplay_desc'] = 'Will the user avatar be shown in popup ?';
$l['abp_umap_ulink'] = 'User link';
$l['abp_umap_ulink_desc'] = 'Link to the user profile in popup ?';
$l['abp_umap_gcolor'] = 'Colorize with group';
$l['abp_umap_gcolor_desc'] = 'Will the username in popup have the color of the main group of the user ?';

// Old settings
$l['abp_umap_setting_title'] = 'Umap settings';
$l['abp_umap_setting_desc'] = 'Various default settings for ABP User Map';

$l['abp_umap_latlng'] = 'Default location';
$l['abp_umap_latlng_desc'] = 'Select the default location for the map by moving the marker';

$l['abp_umap_zoom'] = 'Default zoom level value';
$l['abp_umap_zoom_desc'] = 'Select the zoom value (1 = world, 18 = street details)';

$l['abp_umap_marker'] = 'Marker';
$l['abp_umap_marker_desc'] = 'Marker file location with the leading / (.png / 32px * 32px is quite good)';

$l['abp_umap_btn_save'] = 'Validate';

// Marker position
$l['abp_umap_smarker'] = 'Marker settings';
$l['abp_umap_hmarker'] = 'Horizontal alignment';
$l['abp_umap_hmarker_desc'] = 'Select the horizontal alignment of your marker';
$l['abp_umap_vmarker'] = 'Vertical alignment';
$l['abp_umap_vmarker_desc'] = 'Select the vertical alignment of your marker';
$l['left'] = 'left';
$l['right'] = 'right';
$l['center'] = 'center';
$l['top'] = 'top';
$l['bottom'] = 'bottom';
$l['middle'] = 'middle';