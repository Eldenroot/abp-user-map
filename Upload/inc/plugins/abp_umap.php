<?php

/**
 * Usermap
 * (c) CrazyCat 2019
 */
if (!defined('IN_MYBB')) {
    die('Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.');
}

define('CN_ABPUMAP', str_replace('.php', '', basename(__FILE__)));
// TPL_PREFIX will be used only for templates
// to avoid _ and allows to use group
define('TPL_PREFIX', str_replace('_', '', CN_ABPUMAP));

/**
 * Initialization of the info
 * @global object $lang
 * @return array
 */
function abp_umap_info() {
    global $lang;
    $lang->load(CN_ABPUMAP);
    return array(
        'name' => $lang->abp_umapname,
        'description' => $lang->abp_umapdesc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-user-map',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.2',
        'compatibility' => '18*',
        'codename' => CN_ABPUMAP
    );
}

/**
 * Basics : (de)install, (de)activate
 */

/**
 * Install procedure
 * Adds settings and userdb field
 * @global DB_MySQLi $db
 * @global MyLanguage $lang
 */
function abp_umap_install() {
    global $db, $lang;
    $lang->load(CN_ABPUMAP);
    $settinggroups = ['name' => CN_ABPUMAP,
        'title' => $lang->abp_umap_setting_title,
        'description' => $lang->abp_umap_setting_description,
        'disporder' => 0,
        "isdefault" => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    // New global settings
    $settings = [
        [
            'name' => CN_ABPUMAP . '_gallowed',
            'title' => $lang->abp_umap_gallowed,
            'description' => $lang->abp_umap_gallowed_desc,
            'optionscode' => 'groupselect',
            'value' => '2,3,4,6',
            'disporder' => 1
        ],
        [
            'name' => CN_ABPUMAP . '_adisplay',
            'title' => $lang->abp_umap_adisplay,
            'description' => $lang->abp_umap_adisplay_desc,
            'optionscode' => 'yesno',
            'value' => 1,
            'disporder' => 2
        ],
        [
            'name' => CN_ABPUMAP . '_ulink',
            'title' => $lang->abp_umap_ulink,
            'description' => $lang->abp_umap_ulink_desc,
            'optionscode' => 'yesno',
            'value' => 1,
            'disporder' => 3
        ],
        [
            'name' => CN_ABPUMAP . '_gcolor',
            'title' => $lang->abp_umap_gcolor,
            'description' => $lang->abp_umap_gcolor_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 4
        ],
    ];

    foreach ($settings as $i => $setting) {
        $insert = [
            'name' => $db->escape_string($setting['name']),
            'title' => $db->escape_string($setting['title']),
            'description' => $db->escape_string($setting['description']),
            'optionscode' => $db->escape_string($setting['optionscode']),
            'value' => $db->escape_string($setting['value']),
            'disporder' => $setting['disporder'],
            'gid' => $gid,
        ];
        $db->insert_query('settings', $insert);
    }
    rebuild_settings();

    $db->write_query("ALTER TABLE " . TABLE_PREFIX . "users ADD aum_latlng VARCHAR(50) NULL");
    $db->write_query("CREATE TABLE " . TABLE_PREFIX . CN_ABPUMAP . " ( sname VARCHAR(50) NOT NULL , svalue VARCHAR(200) NOT NULL , UNIQUE sname (sname))");
    $db->insert_query_multiple(CN_ABPUMAP, [
        ['sname' => 'latlng', 'svalue' => '43.3,1.9'],
        ['sname' => 'zoom', 'svalue' => 6],
        ['sname' => 'marker', 'svalue' => '/images/marker.png']
    ]);
}

/**
 * Checks if plugin is installed
 * @global DB_MySQLi $db
 * @return boolean
 */
function abp_umap_is_installed() {
    global $db;
    return ($db->table_exists(CN_ABPUMAP) && $db->field_exists('aum_latlng', 'users'));
}

/**
 * Uninstall function
 * @global DB_MySQLi $db
 */
function abp_umap_uninstall() {
    global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPUMAP . "_%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPUMAP . "'");
    rebuild_settings();
    abp_umap_deactivate();
    $db->write_query("ALTER TABLE " . TABLE_PREFIX . "users DROP COLUMN aum_latlng");
    $db->drop_table(CN_ABPUMAP);
}

/**
 * Activation function : creates the templates
 * @global DB_MySQLi $db
 * @global MyLanguage $lang
 */
function abp_umap_activate() {
    global $db, $lang;
    $db->insert_query('templategroups', ['prefix' => TPL_PREFIX, 'title' => $lang->abp_umapname, 'isdefault' => 1]);
    $templates = [
        [
            'title' => TPL_PREFIX . '_page',
            'template' => '<head>
<title>{$mybb->settings[\\\'bbname\\\']} - ' . $lang->abp_umapname . '</title>
{$headerinclude}
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/MarkerCluster.Default.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.4.1/leaflet.markercluster.js"></script>
</head>
<body>
{$header}
<table border="0" cellspacing="{\$theme[\\\'borderwidth\\\']}" cellpadding="{\$theme[\\\'tablespace\\\']}" class="tborder">
    <tbody>
        <tr><td class="thead"><strong>{\$lang->abp_umap_page_title}</strog></td></tr>
        <tr><td class="trow1"><div id="abp_usermap" style="height:600px;"></div></td></tr>
        <tr><td class="trow1"><ul>
            <li>{\$abp_umap_stats}</li>
            {\$centermelnk}</ul>
        </td></tr>
    </tbody>
</table>
<script type="text/javascript">
	var center = {{$umap_center}};
	var abp_umap = null;
	var markCluster;
	{$umap_users}
	function init_map() {
		abp_umap = L.map("abp_usermap").setView([center.lat, center.lon], {$umap_zoom});
		markCluster = L.markerClusterGroup();
		L.tileLayer("//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {attribution: \\\'{\$lang->osm_credit}\\\'}).addTo(abp_umap);
		for (user in umusers) {
			var uIcon = L.icon({ iconUrl:"{\$mybb->settings[\\\'bburl\\\']}{$umap_marker}", iconSize:[{\$iw}, {\$ih}], iconAnchor: [{\$hof},{\$vof}], popupAnchor :[({\$iw}/2)-{\$hof},-{\$vof}]});
			var marker = L.marker([umusers[user].lat, umusers[user].lon], { icon: uIcon }).bindPopup(umusers[user].img);
			markCluster.addLayer(marker);
		}
		abp_umap.addLayer(markCluster);
	}
	$(function() {
            init_map();
            $("#centerme").click(function() { abp_umap.setView([{\$ulat}, {\$ulng}]); return false; });
        });
</script>
{$footer}
</body>
</html>',
            'sid' => -2,
            'version' => 1.1,
            'dateline' => TIME_NOW
        ],
        [
            'title' => TPL_PREFIX . '_ucp',
            'template' => '<head>
<title>{\$mybb->settings[\\\'bbname\\\']} - {\$lang->abp_umapname}</title>
{$headerinclude}
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js"></script>
</head>
<body>
{$header}
<form name="umap" method="post" action="usercp.php?action=do_abp_umap" />
<input type="hidden" name="ulatlng" id="ulatlng" value="{\$ulatlng}" />
<input type=\"hidden\" name=\"my_post_key\" value=\"{\$mybb->post_code}\" />
<table width="100%" border="0" align="center">
    <tr>
    {\$usercpnav}
    <td align="center" valign="top">
        <table border="0" cellspacing="{\$theme[\\\'borderwidth\\\']}" cellpadding="{\$theme[\\\'tablespace\\\']}" class="tborder">
        <tr><td class="thead"><strong>{\$lang->abp_umapname}</strong></td></tr>
        <tr><td class="tcat"><strong>{\$lang->abp_umap_ucp_notice_title}</strong></td></tr>
        <tr><td class="trow1">{\$abp_umap_location_not_set}{\$lang->abp_umap_ucp_notice}</td></tr>
        <tr><td class="tcat"><strong>{\$lang->abp_umap_title_update}</strong></td></tr>
        <tr><td class="trow1">
        <div id="abp_usermap" style="height:600px;"></div>
        </td></tr>
        <tr><td class="trow1"><strong>{\$lang->abp_umap_confirm}</strong> <input type="checkbox" id="ucheck" name="ucheck" /><label for="ucheck">{\$lang->abp_umap_updateme}</label><input class="button" style="float: right; display: none;" type="submit" value=\"{\$lang->abp_umap_btn_update}\" id="uupdate" /></td></tr>
        <tr><td class="tcat"><strong>{\$lang->abp_umap_title_delete}</strong></td></tr>
        <tr><td class="trow1"><strong>{\$lang->abp_umap_confirm}</strong> <input type="checkbox" id="udel" name="udel" /><label for="udel">{\$lang->abp_umap_deleteme}</label><input class="button" style="float: right; display: none;" type="submit" value=\"{\$lang->abp_umap_btn_delete}\" id="udelete" /></td></tr>
        </table>
    </td>
    </tr>
</table>
</form>
<script type="text/javascript">
    var center = {{$umap_center}};
    var abp_umap = null;
    function init_map() {
        abp_umap = L.map("abp_usermap").setView([center.lat, center.lon], {$umap_zoom});
        L.tileLayer("//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { attribution: \\\'{\$lang->osm_credit}\\\'}).addTo(abp_umap);
		var uIcon = L.icon({ iconUrl:"{\$mybb->settings[\\\'bburl\\\']}{$umap_marker}", iconSize:[{\$iw}, {\$ih}], iconAnchor: [{\$hof},{\$vof}]});
        var userMarker = L.marker([center.lat, center.lon], {draggable:true, icon: uIcon}).addTo(abp_umap).on(\\\'dragend\\\', function() {
            var coord = userMarker.getLatLng();
            $(\\\'#ulatlng\\\').val(coord.lat+\\\',\\\'+coord.lng);
        });
        
    }
    $(function() {
        init_map();
        $("#ucheck").click( function() {
            if ($(this).is(":checked")) { $("#udel").prop("checked", false); $("#udelete").toggle(false); }
            $("#uupdate").toggle($(this).is(":checked"));
            
        });
        $("#udel").click( function() {
            if ($("#ucheck").is(":checked")) { 
                $.jGrowl("{\$lang->abp_umap_cannotdel}", {theme:"jgrowl_error"});
                return false;
            } else {
                $("#udelete").toggle($(this).is(":checked"));
            }
        });
    });
</script>
{$footer}
</body>
</html>',
            'sid' => -2,
            'version' => 1.1,
            'dateline' => TIME_NOW
        ],
        [
            'title' => TPL_PREFIX . '_nav_option',
            'template' => '<tr><td class="trow1 smalltext"><a href="{\$mybb->settings[\\\'bburl\\\']}/usercp.php?action=abp_umap" class="usercp_nav_item" style="background-image: url(\\\'{\$mybb->settings[\\\'bburl\\\']}/images/osm.png\\\'); background-repeat: no-repeat;">{\$lang->abp_umapname}</a></td></tr>',
            'sid' => -2,
            'version' => 1.1,
            'dateline' => TIME_NOW
        ],
        [
            'title' => TPL_PREFIX . '_toplink',
            'template' => '<li><a style="background-image: url(\\\'{\$mybb->settings[\\\'bburl\\\']}/images/osm.png\\\'); background-repeat: no-repeat;" href="{$mybb->settings[\\\'bburl\\\']}/misc.php?action=abp_umap">{\$lang->abp_umapname}</a></li>',
            'sid' => -2,
            'version' => 1.1,
            'dateline' => TIME_NOW
        ]
    ];
    foreach ($templates as $template) {
        $db->insert_query("templates", $template);
    }
    require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
    find_replace_templatesets("usercp_nav_misc", "#" . preg_quote('{$lang->ucp_nav_forum_subscriptions}</a></td></tr>') . "#i", '{$lang->ucp_nav_forum_subscriptions}</a></td></tr>{' . CN_ABPUMAP . '_nav_option}');
    find_replace_templatesets("header", "#" . preg_quote('{$menu_memberlist}') . "#i", '{$menu_memberlist}{$' . CN_ABPUMAP . '_toplink}');
}

/**
 * Deactivation function
 * @global DB_MySQLi $db
 * @global MyLanguage $lang
 */
function abp_umap_deactivate() {
    global $db, $lang;
    $lang->load(CN_ABPUMAP);
    $db->delete_query('templates', "title LIKE '" . TPL_PREFIX . "_%' AND sid=-2");
    $db->delete_query('templategroups', "prefix='" . TPL_PREFIX . "'");
    require_once MYBB_ROOT . '/inc/adminfunctions_templates.php';
    find_replace_templatesets("usercp_nav_misc", "#" . preg_quote('{' . CN_ABPUMAP . '_nav_option}') . "#", '', 0);
    find_replace_templatesets("header", "#" . preg_quote('{$' . CN_ABPUMAP . '_toplink}') . "#", '', 0);
}

$plugins->add_hook("admin_config_menu", "abp_umap_admin_config_menu");

/**
 * Add a sub-menu item in ACP
 * @global MyLanguage $lang
 * @param array $sub_menu
 * @return array
 */
function abp_umap_admin_config_menu($sub_menu) {
    global $lang;
    $lang->load(CN_ABPUMAP);
    $sub_menu[] = [
        'id' => 'abp_umap',
        'title' => $lang->abp_umapname,
        'link' => 'index.php?module=config-' . CN_ABPUMAP
    ];
    return $sub_menu;
}

$plugins->add_hook('admin_config_action_handler', CN_ABPUMAP . '_admin_config_action_handler');

/**
 * 
 * @param type $actions
 */
function abp_umap_admin_config_action_handler(&$actions) {
    $actions[CN_ABPUMAP] = ['active' => CN_ABPUMAP, 'file' => ''];
}

$plugins->add_hook("admin_page_output_header", "abp_umap_admin_header");

function abp_umap_admin_header(&$args) {
    if ($args['this']->active_module == 'config' && $args['this']->active_action == CN_ABPUMAP) {
        $args['this']->extra_header .= '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/leaflet/1.4.0/leaflet.js"></script>' . PHP_EOL;
    }
}

$plugins->add_hook("admin_load", "abp_umap_admin_load");

/**
 * Generates the config page
 * @global object $mybb
 * @global DefaultPage $page
 * @global MyLanguage $lang
 * @global DB_MySQLi $db
 */
function abp_umap_admin_load() {
    global $page, $lang, $db, $mybb;
    if ($mybb->input['module'] != 'config-' . CN_ABPUMAP) {
        return;
    }
    if ($mybb->input['action'] == 'edit' && $mybb->request_method == 'post') {
        if (is_null($mybb->get_input('my_post_key')) || !verify_post_check($mybb->get_input('my_post_key'), true)) {
            flash_message('error verifying', 'alert');
            admin_redirect('index.php?module=config-' . CN_ABPUMAP);
        }
        $nsettings = [];
        list($lat, $lng) = explode(',', $mybb->input['ulatlng']);
        $nsettings['latlng'] = $lat . ',' . $lng;
        $nsettings['zoom'] = (int) $mybb->input['uzoom'];
        $nsettings['marker'] = filter_var($mybb->input['marker'], FILTER_SANITIZE_STRING);
        $nsettings['hmarker'] = filter_var($mybb->input['hmarker'], FILTER_SANITIZE_STRING);
        $nsettings['vmarker'] = filter_var($mybb->input['vmarker'], FILTER_SANITIZE_STRING);
        foreach ($nsettings as $sname => $svalue) {
            $db->replace_query(CN_ABPUMAP, ['svalue' => $svalue, 'sname' => $sname]);
        }
        flash_message('Data saved', 'success');
        admin_redirect('index.php?module=config-' . CN_ABPUMAP);
    }
    $settings = umap_get_settings();
    if (!isset($settings['hmarker'])) {
        $settings['hmarker'] = 'center';
    }
    if (!isset($settings['vmarker'])) {
        $settings['vmarker'] = 'middle';
    }
    list($lat, $lng) = explode(',', $settings['latlng']);
    $lang->load(CN_ABPUMAP);

    // @TODO : create function to get marker size
    $file = str_replace(
            ['/', '\\'], [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR], MYBB_ROOT . $settings['marker']
    );
    list($iw, $ih, $type, $attr) = getimagesize($file);
    // Calculate offset depending on marker settings
    switch ($settings['hmarker']) {
        case 'left': $hof = $iw;
            break;
        case 'right': $hof = 0;
            break;
        default: $hof = $iw / 2;
            break;
    }
    switch ($settings['vmarker']) {
        case 'top': $vof = $ih;
            break;
        case 'bottom': $vof = 0;
            break;
        default: $vof = $ih / 2;
            break;
    }
    $page->add_breadcrumb_item($lang->abp_umapname, 'index.php?module=config-' . CN_ABPUMAP);
    $page->output_header();

    $form = new Form('index.php?module=config-' . CN_ABPUMAP . '&action=edit', 'post');
    $table = new Table();
    $table->construct_header($lang->abp_umapname, ['colspan' => 2]);
    $table->construct_cell('<strong>' . $lang->abp_umap_latlng . '</strong><br />' . $lang->abp_umap_latlng_desc, ['colspan' => 2]);
    $table->construct_row();
    $table->construct_cell($form->generate_hidden_field('ulatlng', (float) $lat . ',' . (float) $lng, ['id' => 'ulatlng']) . PHP_EOL
            . '<div id="abp_usermap" style="height:600px;"></div>' . PHP_EOL
            . '<script type="text/javascript">
    var abp_umap = null;
    function init_map() {
        abp_umap = L.map("abp_usermap").setView([' . (float) $lat . ', ' . (float) $lng . '], ' . $settings['zoom'] . ');
        L.tileLayer("//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", { attribution: \'&copy; <a href="https://www.openstreetmap.org" title="OpenStreetMap">OpenStreetMap</a> contributors\'}).addTo(abp_umap);
        var uIcon = L.icon({ iconUrl:"' . $mybb->settings['bburl'] . $settings['marker'] . '", iconSize:[' . $iw . ', ' . $ih . '], iconAnchor: [' . $hof . ',' . $vof . ']});
        var userMarker = L.marker([' . (float) $lat . ', ' . (float) $lng . '], {draggable:true, icon: uIcon}).addTo(abp_umap).on(\'dragend\', function() {
            var coord = userMarker.getLatLng();
            $(\'#ulatlng\').val(coord.lat+\',\'+coord.lng);
        });
    }
    $(function() { init_map();
        $(\'#uzoom\').change(function() { abp_umap.setZoom($(this).val()); });
        abp_umap.on(\'zoomend\', function() { $(\'#uzoom\').val(abp_umap.getZoom()); });
    });
</script>', ['colspan' => 2]);
    $table->construct_row();
    // Zoom selector
    $table->construct_cell('<strong>' . $lang->abp_umap_zoom . '</strong><br />' . $lang->abp_umap_zoom_desc);
    $table->construct_cell($form->generate_numeric_field('uzoom', $settings['zoom'], ['id' => 'uzoom', 'min' => 1, 'max' => 18]));
    $table->construct_row();
    // Marker icon
    $table->construct_cell('<strong>' . $lang->abp_umap_marker . '</strong><br />' . $lang->abp_umap_marker_desc);
    $table->construct_cell($form->generate_text_box('marker', $settings['marker']));
    $table->construct_row();
    // Marker position
    $table->construct_cell('<strong>' . $lang->abp_umap_hmarker . '</strong><br />' . $lang->abp_umap_hmarker_desc);
    $table->construct_cell(
            $form->generate_select_box('hmarker', ['left' => $lang->left, 'center' => $lang->center, 'right' => $lang->right], $settings['hmarker'], ['id' => 'hmarker'])
    );
    $table->construct_row();
    $table->construct_cell('<strong>' . $lang->abp_umap_vmarker . '</strong><br />' . $lang->abp_umap_vmarker_desc);
    $table->construct_cell(
            $form->generate_select_box('vmarker', ['top' => $lang->top, 'middle' => $lang->middle, 'bottom' => $lang->bottom], $settings['vmarker'], ['id' => 'vmarker'])
    );
    $table->construct_row();
    $table->output();
    $buttons = [];
    $buttons[] = $form->generate_submit_button($lang->abp_umap_btn_save);
    $form->output_submit_wrapper($buttons);
    $form->end();
    $page->output_footer();
}

//$plugins->add_hook('admin_config_settings_change_commit', CN_ABPUMAP . '_setting_update');
// Will I need to do something after updating settings ?


/**
 * @TODO : Add user info country / town => in usercp to allow a display of map
 * => use geonames ?
 */
/** User cp * */
$plugins->add_hook('usercp_menu_built', 'abp_umap_navoption', -10);

/**
 * Creates submenu
 * @global MyLanguage $lang
 */
function abp_umap_navoption() {
    global $mybb, $usercpnav, $lang, $templates, $templatelist, $usercpnav, $abp_umap_nav_option;
    $lang->load(CN_ABPUMAP);
    if ((string) $templatelist != '') {
        $templatelist .= ' ,';
    }
    $templatelist .= TPL_PREFIX . '_nav_option';
    eval("\$abp_umap_nav_option = \"" . $templates->get(TPL_PREFIX . '_nav_option') . "\";");
    $usercpnav = str_replace("{abp_umap_nav_option}", $abp_umap_nav_option, $usercpnav);
}

$plugins->add_hook('usercp_start', 'abp_umap_usercp');

/**
 * Creates UserCP page
 * @global MyLanguage $lang
 */
function abp_umap_usercp() {
    global $mybb, $db, $cache, $lang, $theme, $templates, $templatelist, $headerinclude, $header, $footer, $usercpnav;
    if ((string) $templatelist != '') {
        $templatelist .= ' ,';
    }
    $templatelist .= TPL_PREFIX . '_ucp';
    $lang->load(CN_ABPUMAP);
    if ($mybb->input['action'] == 'do_abp_umap' && (isset($mybb->input['ucheck']) || isset($mybb->input['udel']) )) {
        verify_post_check($mybb->input['my_post_key']);
        if (isset($mybb->input['ucheck']) && $mybb->input['ucheck'] == 'on') {
            list($lat, $lng) = explode(',', $mybb->input['ulatlng']);
            $latlng = (float) $lat . ',' . (float) $lng;
            $message = $lang->abp_umap_updated;
        } elseif (isset($mybb->input['udel']) && $mybb->input['udel'] == 'on') {
            $latlng = null;
            $message = $lang->abp_umap_deleted;
        }
        $db->update_query('users', ['aum_latlng' => $latlng], "uid=" . (int) $mybb->user['uid']);
        redirect('usercp.php?action=abp_umap', $message);
    } elseif ($mybb->input['action'] == 'do_abp_umap' && !isset($mybb->input['ucheck']) && !isset($mybb->input['udel'])) {
        redirect('usercp.php?action=abp_umap', $lang->abp_umap_what);
    } elseif ($mybb->input['action'] == 'abp_umap') {
        add_breadcrumb($lang->nav_usercp, "usercp.php");
        add_breadcrumb($lang->abp_umapname, "usercp.php?action=abp_umap");
        $usettings = umap_get_settings();
        $file = str_replace(
                ['/', '\\'], [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR], MYBB_ROOT . $usettings['marker']
        );
        list($iw, $ih, $type, $attr) = getimagesize($file);
        $umap_zoom = $usettings['zoom'];
        $umap_marker = $usettings['marker'];
        switch ($usettings['hmarker']) {
            case 'left': $hof = $iw;
                break;
            case 'right': $hof = 0;
                break;
            default: $hof = $iw / 2;
                break;
        }
        switch ($usettings['vmarker']) {
            case 'top': $vof = $ih;
                break;
            case 'bottom': $vof = 0;
                break;
            default: $vof = $ih / 2;
                break;
        }
        if (isset($mybb->user['aum_latlng']) && (string) $mybb->user['aum_latlng'] != '') {
            list($lat, $lng) = explode(',', $mybb->user['aum_latlng']);
            $abp_umap_location_not_set = '';
        } else {
            list($lat, $lng) = explode(',', $usettings['latlng']);
            $abp_umap_location_not_set = $lang->abp_umap_location_not_set;
        }
        $umap_center = "lat:" . (float) $lat . ", lon:" . (float) $lng;
        $ulatlng = (float) $lat . ',' . (float) $lng;
        eval("\$page = \"" . $templates->get(TPL_PREFIX . '_ucp') . "\";");
        output_page($page);
    }
}

/** Usermap page * */
$plugins->add_hook('misc_start', 'abp_umap');

/**
 * Creates the usermap page
 * @global MyLanguage $lang
 */
function abp_umap() {
    global $mybb, $templates, $theme, $templatelist, $lang, $header, $headerinclude, $footer, $db;

    if ($mybb->input['action'] != CN_ABPUMAP) {
        return;
    }
    if ((string) $templatelist != '') {
        $templatelist .= ', ';
    }
    $templatelist .= TPL_PREFIX . '_page';

    if (!check_perms($mybb->settings[CN_ABPUMAP . '_gallowed'], $mybb->user)) {
        error_no_permission();
    }

    $lang->load(CN_ABPUMAP);
    $query = $db->simple_select('users', 'uid, username, usergroup, displaygroup, avatar, aum_latlng', "aum_latlng is not null and aum_latlng<>''");

    $ulat = $ulng = 0;
    $cptusers = 0;
    $centermelnk = '';
    while ($user = $db->fetch_array($query)) {
        $cptusers++;
        list($lat, $lng) = explode(',', $user['aum_latlng']);
        $udata[$user['username']] = [
            'lat' => (float) $lat,
            'lon' => (float) $lng,
            'img' => abp_make_popup($user)
        ];
        if ($user['uid'] == $mybb->user['uid']) {
            $ulat = (float) $lat;
            $ulng = (float) $lng;
            $centermelnk = $lang->abp_umap_centerme;
        }
    }
    $usettings = umap_get_settings();
    $umap_zoom = $usettings['zoom'];

    // Getting marker info
    $file = str_replace(
            ['/', '\\'], [DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR], MYBB_ROOT . $usettings['marker']
    );

    list($iw, $ih, $type, $attr) = getimagesize($file);
    // @TODO : $udata = $cache->read('abp_umap');
    list($lat, $lng) = explode(',', $usettings['latlng']);
    $umap_center = "lat:" . (float) $lat . ", lon:" . (float) $lng;
    $umap_users = 'var umusers = ' . json_encode($udata);
    $umap_marker = $usettings['marker'];
    switch ($usettings['hmarker']) {
        case 'left': $hof = $iw;
            break;
        case 'right': $hof = 0;
            break;
        default: $hof = $iw / 2;
            break;
    }
    switch ($usettings['vmarker']) {
        case 'top': $vof = $ih;
            break;
        case 'bottom': $vof = 0;
            break;
        default: $vof = $ih / 2;
            break;
    }
    $abp_umap_stats = $lang->sprintf($lang->abp_umap_stats, $cptusers);
    if ($mybb->get_input('action') == 'abp_umap') {
        add_breadcrumb($lang->abp_umapname, "misc.php?action=abp_umap");
        eval("\$page = \"" . $templates->get(TPL_PREFIX . '_page') . "\";");
        output_page($page);
    }
}

/**
 * Format the popup, based on settings
 * @global MyBB $mybb
 * @param array $user
 * @return string
 */
function abp_make_popup($user) {
    global $mybb;
    $img = '';
    if ($mybb->settings['abp_umap_adisplay'] == 1 && trim($user['avatar']) != '') {
        $img = '<img src="' . $user['avatar'] . '" /><br />';
    }
    if ($mybb->settings['abp_umap_gcolor'] == 1) {
        $img .= format_name(htmlspecialchars_uni($user['username']), $user['usergroup'], $user['displaygroup']);
    } else {
        $img .= $user['username'];
    }
    if ($mybb->settings['abp_umap_ulink'] == 1) {
        $img = '<a href="' . get_profile_link($user['uid']) . '" title="' . $user['username'] . '">' . $img . '</a>';
    }
    return $img;
}

$plugins->add_hook('global_start', 'abp_umap_toplink');

/**
 * Add the top link
 * @global type $mybb
 * @global type $templates
 * @global MyLanguage $lang
 * @global type $abp_umap_toplink
 */
function abp_umap_toplink() {
    global $mybb, $templates, $templatelist, $lang, $abp_umap_toplink;
    $lang->load(CN_ABPUMAP);
    if ((string) $templatelist != '') {
        $templatelist .= ' ,';
    }
    $templatelist .= TPL_PREFIX . '_toplink';
    eval("\$" . CN_ABPUMAP . "_toplink = \"" . $templates->get(TPL_PREFIX . '_toplink') . "\";");
}

/** WOL * */
$plugins->add_hook('fetch_wol_activity_end', 'abp_umap_wol');
$plugins->add_hook('build_friendly_wol_location_end', 'abp_umap_build_wol');

/**
 * 
 * @global type $user
 * @global type $mybb
 * @param string $user_activity
 */
function abp_umap_wol(&$user_activity) {
    global $user, $mybb;
    if (my_strpos($user['location'], 'abp_umap') !== false) {
        $user_activity['activity'] = 'Usermap';
    }
}

/**
 * 
 * @global type $mybb
 * @global MyLanguage $lang
 * @param type $plugin_array
 */
function abp_umap_build_wol(&$plugin_array) {
    global $mybb, $lang;
    if ($plugin_array['user_activity']['activity'] == 'Usermap') {
        $lang->load(CN_ABPUMAP);
        $plugin_array['location_name'] = $lang->sprintf($lang->abp_umap_wol, 'misc.php?action=abp_umap');
    }
}

function umap_get_settings() {
    global $db;
    $settings = [];
    $query = $db->simple_select(CN_ABPUMAP);
    while ($setting = $db->fetch_array($query)) {
        $settings[$setting['sname']] = $setting['svalue'];
    }
    return $settings;
}

/**
 * Checks user perms against a group selection
 * @param string $groups
 * @param array $user
 * @return boolean
 */
function check_perms($groups, $user) {
    $check = false;
    switch ($groups) {
        case '-1':
        case 'all':
            $check = true;
            break;
        case '':
            $check = false;
            break;
        default:
            $guser = array_merge(array($user['usergroup']), explode(',', $user['additionalgroups']));
            if (count(array_filter(array_intersect(explode(',', $groups), $guser))) > 0) {
                $check = true;
            }
            break;
    }
    return $check;
}

/**
 * Actually not used
 * @global DB_MySQLi $db
 * @global type $cache
 */
function abp_umap_cache() {
    global $db, $cache;
    $query = $db->simple_select('users', 'username, avatar, aum_latlng', 'aum_latlng is not null');
    $usermap = array();
    while ($user = $db->fetch_array($query)) {
        list($lat, $lon) = explode(',', $user['aum_latlng']);
        $usermap[$user['username']] = [
            'lat' => (float) $lat,
            'lon' => (float) $lon,
            'img' => '<img src="' . $user['avatar'] . '" title="' . $user['username'] . '"  alt="' . $user['username'] . '" />'
        ];
    }
    $cache->update('abp_umap', $usermap);
}
